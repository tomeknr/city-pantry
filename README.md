## Business and technical informations
* I used more official regex to validate postcode. Provided by you is incorrect. My source: https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/488478/Bulk_Data_Transfer_-_additional_validation_valid_from_12_November_2015.pdf
* I didn't have time to validate provided file. I'm aware that on the production we can't trust incoming sources so we should validate both: format and values (for example ";" in the value will cause a problem).
* It would be nice to have any logging mechanism.
* By default console application should have any --help option which allows users to see definition of command. There is no such mechanism here.
* I written some test, but because of lack of time not everything has been covered. I hope this will be enough for you.
* Application doesn't support time change in leap year
* I decided to not use any database. My solution based on the parsing file line by line and according to the business requirements build collection of results.
* Application has been created and tested on PHP 7.2.12. I can't guarantee that on the different PHP version everything will be ok.
* There is a bug in documentation: `that takes four input parameters` -> there are five parameters.

## Run application
* clone repository
* go to the root folder
* composer install
* run `php /bin/console.php {path} {day} {time} {location} {covers}`

## Run tests
* go to the root folder
* run `./vendor/bin/phpunit --bootstrap vendor/autoload.php tests`

## Code style (PHP-CS-Fixer)
* go to the root folder
* run dry-run: `php vendor/bin/php-cs-fixer fix ./src --dry-run`
* run: `php vendor/bin/php-cs-fixer fix ./src`
* if you want to use this with tests just change the path from `./src` to `./tests`
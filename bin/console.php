<?php

require './vendor/autoload.php';

$commandManager = (new \App\Factory\Service\CommandManagerFactory())->getInstance($argv);
$commandManager->handle();
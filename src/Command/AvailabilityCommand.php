<?php

declare(strict_types=1);

namespace App\Command;

use App\DataProvider\DataProviderInterface;
use App\Exception\FileException;
use App\Exception\LackOfParameterException;
use App\Filter\Item\ItemFilter;
use App\Input\InputInterface;
use App\Output\Output;
use App\Output\OutputInterface;
use App\Parameter\ParameterBagInterface;
use App\Parameter\ParameterBag;
use App\Filter\Vendor\VendorFilter;
use App\Service\ValidationServiceInterface;

final class AvailabilityCommand implements CommandInterface
{
    /** @var ValidationService */
    private $validation;

    /** @var DataProviderInterface */
    private $dataManager;

    /**
     * @param ValidationServiceInterface $validation
     * @param DataProviderInterface $dataManager
     */
    public function __construct(ValidationServiceInterface $validation, DataProviderInterface $dataManager)
    {
        $this->validation = $validation;
        $this->dataManager = $dataManager;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function run(InputInterface $input, OutputInterface $output): void
    {
        // build parameters bag for this command
        $parameterBag = $this->generateParameterBag($input);

        // validate parameters
        if (!$this->validation->validate($parameterBag)) {
            $output->printCollection($this->validation->getMessages(), Output::ERROR_TYPE);

            return;
        }

        // search in file and handle the exceptions
        try {
            $results = $this->dataManager->search($parameterBag->get('path'), $parameterBag);
            $output->printFormattedCollection($results);
        } catch (LackOfParameterException | FileException $exception) {
            $output->print($exception->getMessage(), Output::ERROR_TYPE);
        } catch (\Throwable $exception) {
            $output->print('There was unexpected exception', Output::ERROR_TYPE);
        }
    }

    /**
     * @param InputInterface $input
     * @return ParameterBagInterface
     *
     * TODO ParameterBag is shared with too many classes. Too many dependencies
     *
     * For this simple application it is good enough, but this is a place to the future refactor
     */
    private function generateParameterBag(InputInterface $input): ParameterBagInterface
    {
        $parameterBag = new ParameterBag();

        return $parameterBag
            ->set('path', $input->getArgument(1))
            ->set(ItemFilter::PARAMETER_DATE, $input->getArgument(2))
            ->set(ItemFilter::PARAMETER_TIME, $input->getArgument(3))
            ->set(VendorFilter::PARAMETER_POSTCODE, $input->getArgument(4))
            ->set(VendorFilter::PARAMETER_COVERS, $input->getArgument(5));
    }
}

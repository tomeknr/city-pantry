<?php

declare(strict_types=1);

namespace App\Command;

use App\Input\InputInterface;
use App\Output\OutputInterface;

interface CommandInterface
{
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function run(InputInterface $input, OutputInterface $output): void;
}

<?php

declare(strict_types=1);

namespace App\DataProvider;

use App\Parameter\ParameterBagInterface;

interface DataProviderInterface
{
    /**
     * @param string $path
     * @param ParameterBagInterface $parameterBag
     * @return array
     */
    public function search(string $path, ParameterBagInterface $parameterBag): array;
}

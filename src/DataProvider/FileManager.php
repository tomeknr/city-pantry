<?php

declare(strict_types=1);

namespace App\DataProvider;

use App\DataProvider\Handler\DataProviderHandler;
use App\Exception\FileException;
use App\Filter\Item\ItemFilter;
use App\Filter\Vendor\VendorFilter;
use App\Parameter\ParameterBagInterface;

final class FileManager implements DataProviderInterface
{
    /** @var HandlerInterface  */
    private $handler;
    /** @var VendorFilter  */
    private $vendorFilterManager;
    /** @var ItemFilter  */
    private $itemFilterManager;

    /**
     * @param DataProviderHandler $handler
     * @param VendorFilter $vendorFilterManager
     * @param ItemFilter $itemFilterManager
     */
    public function __construct(
        DataProviderHandler $handler,
        VendorFilter $vendorFilterManager,
        ItemFilter $itemFilterManager
    ) {
        $this->handler = $handler;
        $this->vendorFilterManager = $vendorFilterManager;
        $this->itemFilterManager = $itemFilterManager;

        $this->handler->setVendorFilterManager($vendorFilterManager);
        $this->handler->setItemFilterManager($itemFilterManager);
    }


    /**
     * @param string $path
     * @param ParameterBagInterface $parameterBag
     * @return array
     * @throws \Throwable
     *
     * Here is a simple way to iterate line by line and work with each line separately.
     * This will be work proper with all simple text formats which can be parsed line by line (txt, csv).
     * If we want to add support for more complex file like .xlsx we should add here some abstraction.
     */
    public function search(string $path, ParameterBagInterface $parameterBag): array
    {
        // this is not the best solutions, but I want to be sure that resource will be closed
        try {
            $handle = fopen($path, "r");

            if (!$handle) {
                throw new FileException($path);
            }

            while (($buffer = fgets($handle, 4096)) !== false) {
                $this->handler->handleFile($buffer, $parameterBag);
            }

            fclose($handle);
        } catch (\Throwable $exception) {
            fclose($handle);
            throw $exception;
        }

        return $this->handler->getCollection();
    }
}

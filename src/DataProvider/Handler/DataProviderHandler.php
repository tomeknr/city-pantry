<?php

declare(strict_types=1);

namespace App\DataProvider\Handler;

use App\Entity\Item;
use App\Entity\Vendor;
use App\Filter\Item\ItemFilter;
use App\Filter\Vendor\VendorFilter;
use App\Parameter\ParameterBagInterface;

abstract class DataProviderHandler
{
    /** @var VendorFilter */
    protected $vendorFilterManager;
    /** @var ItemFilter */
    protected $itemFilterManager;
    /** @var Vendor[]  */
    protected $collection = [];

    /**
     * @param string $data
     * @param ParameterBagInterface $parameterBag
     */
    abstract public function handleFile(string $data, ParameterBagInterface $parameterBag): void;

    /**
     * @param VendorFilter $vendorFilterManager
     */
    public function setVendorFilterManager(VendorFilter $vendorFilterManager): void
    {
        $this->vendorFilterManager = $vendorFilterManager;
    }

    /**
     * @param ItemFilter $itemFilterManager
     */
    public function setItemFilterManager(ItemFilter $itemFilterManager): void
    {
        $this->itemFilterManager = $itemFilterManager;
    }

    /**
     * @return array
     */
    public function getCollection(): array
    {
        return $this->collection;
    }

    /**
     * @param Vendor $vendor
     * @param ParameterBagInterface $parameterBag
     * @return bool
     */
    protected function checkVendor(Vendor $vendor, ParameterBagInterface $parameterBag): bool
    {
        if (isset($this->vendorFilterManager)) {
            return $this->vendorFilterManager->filter($vendor, $parameterBag);
        }

        return true;
    }

    /**
     * @param Item $item
     * @param ParameterBagInterface $parameterBag
     * @return bool
     */
    protected function checkItem(Item $item, ParameterBagInterface $parameterBag): bool
    {
        if (isset($this->itemFilterManager)) {
            return $this->itemFilterManager->filter($item, $parameterBag);
        }

        return true;
    }

    /**
     * @param string $name
     * @param string $postcode
     * @param int $maxCovers
     * @return Vendor
     *
     * TODO If we need to generate object also in different place, move to fabric
     */
    protected function generateVendor(string $name, string $postcode, int $maxCovers): Vendor
    {
        return (new Vendor())
            ->setName($name)
            ->setPostcode($postcode)
            ->setMaxCovers($maxCovers);
    }

    /**
     * @param string $name
     * @param array $allergies
     * @param string $advanceTime
     * @return Item
     *
     * TODO If we need to generate object also in different place, move to fabric
     */
    protected function generateItem(string $name, array $allergies, string $advanceTime): Item
    {
        return (new Item())
            ->setName($name)
            ->setAllergies($allergies)
            ->setAdvanceTime($advanceTime);
    }
}

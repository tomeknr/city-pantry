<?php

declare(strict_types=1);

namespace App\DataProvider\Handler;

use App\Entity\Vendor;
use App\Parameter\ParameterBagInterface;

/**
 * Class has to change incoming strings to objects.
 * This handler can work only with EBNF-like text format (according to documentation)
 *
 * TODO File should be validated. Each DataProviderHandler class should have own format and value validator
 *
 * I didn't have time to validate the structure and values in the file. On the production this has to be fixed.
 */
final class EbnfFileHandler extends DataProviderHandler
{
    /**
     * @var bool
     */
    private $isVendor = true;
    /**
     * @var Vendor|null
     */
    private $currentVendor;

    /**
     * @param string $data
     * @param ParameterBagInterface $parameterBag
     */
    public function handleFile(string $data, ParameterBagInterface $parameterBag): void
    {
        if (trim($data) === '') {
            // reset current Vendor
            $this->currentVendor = null;
            // the next row will be a Vendor
            $this->isVendor = true;

            return;
        }

        $data = explode(';', $data);

        if ($this->isVendor) {
            // do this when we have Vendor line
            $this->handleVendor($data, $parameterBag);
        } elseif ($this->currentVendor instanceof Vendor) {
            // do this when we have Item line and Vendor is correct so we can add Item to Vendor
            $this->handleItem($data, $parameterBag);
        }
    }

    /**
     * @param array $data
     * @param ParameterBagInterface $parameterBag
     */
    private function handleVendor(array $data, ParameterBagInterface $parameterBag): void
    {
        // reset Vendor line -> the next one will be Item line
        $this->isVendor = false;
        [$name, $postcode, $covers] = $data;

        $newVendor = $this->generateVendor($name, $postcode, (int) $covers);

        // whole business magic is here
        if (!$this->checkVendor($newVendor, $parameterBag)) {
            return;
        }

        // if Vendor is proper we will set it as current. This allow us to add Items to it
        $this->currentVendor = $newVendor;
    }

    /**
     * @param array $data
     * @param ParameterBagInterface $parameterBag
     */
    private function handleItem(array $data, ParameterBagInterface $parameterBag): void
    {
        [$name, $allergies, $advanceTime] = $data;

        $allergies = empty($allergies) ? [] : explode(',', $allergies);
        $item = $this->generateItem($name, $allergies, $advanceTime);

        // whole business magic is here
        if (!$this->checkItem($item, $parameterBag)) {
            return;
        }

        $this->currentVendor->addItem($item);

        $this->addVendorToCollection();
    }

    /**
     * If we have at least one Item in Vendor we will add Vendor object to collection
     */
    private function addVendorToCollection()
    {
        if ($this->currentVendor instanceof Vendor && count($this->currentVendor->getItems()) === 1) {
            $this->collection[] = $this->currentVendor;
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

/**
 * @codeCoverageIgnore
 */
class Item
{
    /** @var string */
    private $name;

    /** @var array */
    private $allergies = [];

    /** @var string */
    private $advanceTime;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Item
     */
    public function setName(string $name): Item
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return array
     */
    public function getAllergies(): array
    {
        return $this->allergies;
    }

    /**
     * @param array $allergies
     * @return Item
     */
    public function setAllergies(array $allergies): Item
    {
        $this->allergies = $allergies;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdvanceTime(): string
    {
        return $this->advanceTime;
    }

    /**
     * @param string $advanceTime
     * @return Item
     */
    public function setAdvanceTime(string $advanceTime): Item
    {
        $this->advanceTime = $advanceTime;

        return $this;
    }
}

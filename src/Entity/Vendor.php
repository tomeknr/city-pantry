<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Item;

/**
 * @codeCoverageIgnore
 */
class Vendor
{
    /** @var string */
    private $name;

    /** @var string */
    private $postcode;

    /** @var int */
    private $maxCovers;

    /** @var Item[] */
    private $items = [];

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Vendor
     */
    public function setName(string $name): Vendor
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     * @return Vendor
     */
    public function setPostcode(string $postcode): Vendor
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxCovers(): int
    {
        return $this->maxCovers;
    }

    /**
     * @param int $maxCovers
     * @return Vendor
     */
    public function setMaxCovers(int $maxCovers): Vendor
    {
        $this->maxCovers = $maxCovers;

        return $this;
    }

    /**
     * @return Item[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param Item $item
     * @return Vendor
     */
    public function addItem(Item $item): Vendor
    {
        $this->items[] = $item;

        return $this;
    }
}

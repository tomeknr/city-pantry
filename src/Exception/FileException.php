<?php

declare(strict_types=1);

namespace App\Exception;

/**
 * @codeCoverageIgnore
 */
final class FileException extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'File: %s cannot be open';

    public function __construct($path, $code = 0, Exception $previous = null)
    {
        $message = sprintf($this->message, $path);
        parent::__construct($message, $code, $previous);
    }
}

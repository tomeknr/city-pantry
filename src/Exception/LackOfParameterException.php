<?php

declare(strict_types=1);

namespace App\Exception;

/**
 * @codeCoverageIgnore
 */
final class LackOfParameterException extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'Lack of required parameter';
}

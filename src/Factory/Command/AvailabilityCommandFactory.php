<?php

declare(strict_types=1);

namespace App\Factory\Command;

use App\Command\AvailabilityCommand;
use App\Factory\FactoryInterface;
use App\Factory\Service\ValidationServiceFactory;
use App\Factory\Service\FileManagerFactory;

final class AvailabilityCommandFactory implements FactoryInterface
{
    /**
     * @param mixed ...$params
     * @return AvailabilityCommand
     */
    public function getInstance(...$params): AvailabilityCommand
    {
        return new AvailabilityCommand(
            (new ValidationServiceFactory)->getInstance(),
            (new \App\Factory\DataProvider\FileManagerFactory())->getInstance()
        );
    }
}

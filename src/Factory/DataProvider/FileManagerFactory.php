<?php

declare(strict_types=1);

namespace App\Factory\DataProvider;

use App\DataProvider\FileManager;
use App\DataProvider\Handler\EbnfFileHandler;
use App\Factory\FactoryInterface;
use App\Factory\Filter\ItemFilterManagerFactory;
use App\Factory\Filter\VendorFilterManagerFactory;

final class FileManagerFactory implements FactoryInterface
{
    /**
     * @param mixed ...$params
     * @return FileManager
     */
    public function getInstance(...$params): FileManager
    {
        $handler = new EbnfFileHandler();

        return new FileManager(
            $handler,
            (new VendorFilterManagerFactory())->getInstance(),
            (new ItemFilterManagerFactory())->getInstance()
        );
    }
}

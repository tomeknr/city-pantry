<?php

declare(strict_types=1);

namespace App\Factory;

interface FactoryInterface
{
    /**
     * @param mixed ...$params
     * @return object
     */
    public function getInstance(...$params);
}

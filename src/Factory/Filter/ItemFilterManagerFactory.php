<?php

declare(strict_types=1);

namespace App\Factory\Filter;

use App\Filter\Item\EnoughTimeFilter;
use App\Filter\ItemFilterManager;
use App\Factory\FactoryInterface;

final class ItemFilterManagerFactory implements FactoryInterface
{
    /**
     * @param mixed ...$params
     * @return ItemFilterManager
     */
    public function getInstance(...$params): ItemFilterManager
    {
        $manager = new ItemFilterManager();
        $manager->addFilter(new EnoughTimeFilter(new \DateTime()));

        return $manager;
    }
}

<?php

declare(strict_types=1);

namespace App\Factory\Filter;

use App\Factory\FactoryInterface;
use App\Filter\VendorFilterManager;
use App\Filter\Vendor\CoverFilter;
use App\Filter\Vendor\LocationFilter;

final class VendorFilterManagerFactory implements FactoryInterface
{
    /**
     * @param mixed ...$params
     * @return VendorFilterManager
     */
    public function getInstance(...$params): VendorFilterManager
    {
        $manager = new VendorFilterManager();
        $manager->addFilter(new LocationFilter());
        $manager->addFilter(new CoverFilter());

        return $manager;
    }
}

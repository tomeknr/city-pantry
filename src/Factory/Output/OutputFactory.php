<?php

declare(strict_types=1);

namespace App\Factory\Output;

use App\Factory\FactoryInterface;
use App\Output\Format\SimpleItemListFormat;
use App\Output\Output;

final class OutputFactory implements FactoryInterface
{
    /**
     * @param mixed ...$params
     * @return Output
     */
    public function getInstance(...$params): Output
    {
        return new Output(new SimpleItemListFormat());
    }
}

<?php

declare(strict_types=1);

namespace App\Factory\Service;

use App\Factory\FactoryInterface;
use App\Factory\Output\OutputFactory;
use App\Input\Input;
use App\Service\CommandManager;

final class CommandManagerFactory implements FactoryInterface
{
    /**
     * @param mixed ...$params
     * @return CommandManager
     */
    public function getInstance(...$params): CommandManager
    {
        $input  = new Input($params[0]);
        $output = (new OutputFactory())->getInstance();

        return new CommandManager($input, $output);
    }
}

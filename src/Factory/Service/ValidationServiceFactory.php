<?php

declare(strict_types=1);

namespace App\Factory\Service;

use App\Factory\FactoryInterface;
use App\Factory\Validation\AvailabilityValidationCollectionFactory;
use App\Factory\Validation\ValidationManagerFactory;
use App\Service\ValidationService;

final class ValidationServiceFactory implements FactoryInterface
{
    /**
     * @param mixed ...$params
     * @return ValidationService
     */
    public function getInstance(...$params): ValidationService
    {
        return new ValidationService(
            (new ValidationManagerFactory())->getInstance(),
            (new AvailabilityValidationCollectionFactory())->getInstance()
        );
    }
}

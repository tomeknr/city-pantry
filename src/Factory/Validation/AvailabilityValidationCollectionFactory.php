<?php

declare(strict_types=1);

namespace App\Factory\Validation;

use App\Factory\FactoryInterface;
use App\Filter\Item\ItemFilter;
use App\Filter\Vendor\VendorFilter;
use App\Validation\ValidationVariable;
use App\Validation\ValidationVariableCollection;
use App\Validation\Type\DateTimeFormatValidator;
use App\Validation\Type\NumberValidator;
use App\Validation\Type\RegexValidator;
use App\Validation\Type\RequiredValidator;
use App\Validation\Type\File\ReadableFileValidator;
use App\Validation\Type\File\RegularFileValidator;

final class AvailabilityValidationCollectionFactory implements FactoryInterface
{
    /**
     * @param mixed ...$params
     * @return ValidationVariableCollection
     *
     * Here we decide what kind of parameters we want to use in specific command.
     */
    public function getInstance(...$params): ValidationVariableCollection
    {
        $collection = new ValidationVariableCollection();

        // key is the number of the parameter
        $config = [
            'path' => [
                new RequiredValidator(),
                new ReadableFileValidator(),
                new RegularFileValidator(),
            ],
            ItemFilter::PARAMETER_DATE => [
                new RequiredValidator(),
                new DateTimeFormatValidator(DateTimeFormatValidator::DEFAULT_DATE_FORMAT, 'date is in incorrect format')
            ],
            ItemFilter::PARAMETER_TIME => [
                new RequiredValidator(),
                new DateTimeFormatValidator(DateTimeFormatValidator::DEFAULT_TIME_FORMAT, 'time is in incorrect format')
            ],
            VendorFilter::PARAMETER_POSTCODE => [
                new RequiredValidator(),
                new RegexValidator(RegexValidator::POSTCODE_FORMAT, 'postcode is in incorrect format')
            ],
            VendorFilter::PARAMETER_COVERS => [
                new RequiredValidator,
                new NumberValidator()
            ],
        ];

        foreach ($config as $name => $validators) {
            $parameterSettings = new ValidationVariable();
            $parameterSettings
                ->setName($name)
                ->setValidators($validators);

            $collection->add($parameterSettings);
        }

        return $collection;
    }
}

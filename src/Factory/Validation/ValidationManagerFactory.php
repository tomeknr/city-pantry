<?php

declare(strict_types=1);

namespace App\Factory\Validation;

use App\Factory\FactoryInterface;
use App\Validation\ValidationManager;

final class ValidationManagerFactory implements FactoryInterface
{
    /**
     * @param mixed ...$params
     * @return ValidationManager
     */
    public function getInstance(...$params): ValidationManager
    {
        return new ValidationManager();
    }
}

<?php

declare(strict_types=1);

namespace App\Filter\Item;

use App\Entity\Item;
use App\Exception\LackOfParameterException;
use App\Parameter\ParameterBagInterface;
use App\Validation\Type\DateTimeFormatValidator;

final class EnoughTimeFilter extends ItemFilter
{
    /** @var \DateTime  */
    private $currentDate;

    /**
     * @param \DateTime|null $currentDate
     */
    public function __construct(\DateTime $currentDate)
    {
        $this->currentDate = $currentDate;
    }

    /**
     * @param Item $item
     * @param ParameterBagInterface $parameterBag
     * @return bool
     * @throws LackOfParameterException
     */
    public function filter(Item $item, ParameterBagInterface $parameterBag): bool
    {
        $date = $parameterBag->get(ItemFilter::PARAMETER_DATE);
        $time = $parameterBag->get(ItemFilter::PARAMETER_TIME);

        if (!isset($date) || !isset($time)) {
            throw new LackOfParameterException();
        }

        $pattern = DateTimeFormatValidator::DEFAULT_DATE_FORMAT . ' ' . DateTimeFormatValidator::DEFAULT_TIME_FORMAT;
        $whenDelivery = \DateTime::createFromFormat($pattern, $date . ' ' . $time);

        $interval = $whenDelivery->diff($this->currentDate);
        $diffHours = $interval->h + ($interval->d * 24);

        // not enough time to handle
        if ($this->currentDate > $whenDelivery || (int) $item->getAdvanceTime() > $diffHours) {
            return false;
        }

        return true;
    }
}

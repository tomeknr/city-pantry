<?php

declare(strict_types=1);

namespace App\Filter\Item;

use App\Entity\Item;
use App\Parameter\ParameterBagInterface;

abstract class ItemFilter
{
    /** @var string  */
    public const PARAMETER_DATE = 'date';
    /** @var string  */
    public const PARAMETER_TIME = 'time';

    /**
     * @param Item $item
     * @param ParameterBagInterface $parameterBag
     * @return bool
     */
    abstract public function filter(Item $item, ParameterBagInterface $parameterBag): bool;
}

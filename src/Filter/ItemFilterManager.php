<?php

declare(strict_types=1);

namespace App\Filter;

use App\Entity\Item;
use App\Filter\Item\ItemFilter;
use App\Parameter\ParameterBagInterface;

final class ItemFilterManager extends ItemFilter
{
    /** @var ItemFilter[] */
    private $collection = [];

    public function addFilter(ItemFilter $filter): void
    {
        $this->collection[] = $filter;
    }

    /**
     * @param Item $item
     * @param ParameterBagInterface $parameterBag
     * @return bool
     */
    public function filter(Item $item, ParameterBagInterface $parameterBag): bool
    {
        foreach ($this->collection as $filter) {
            if (!$filter->filter($item, $parameterBag)) {
                return false;
            }
        }

        return true;
    }
}

<?php

declare(strict_types=1);

namespace App\Filter\Vendor;

use App\Entity\Vendor;
use App\Exception\LackOfParameterException;
use App\Parameter\ParameterBagInterface;

final class CoverFilter extends VendorFilter
{
    /**
     * @param Vendor $vendor
     * @param ParameterBagInterface $parameterBag
     * @return bool
     * @throws LackOfParameterException
     */
    public function filter(Vendor $vendor, ParameterBagInterface $parameterBag): bool
    {
        $covers = (int) $parameterBag->get(VendorFilter::PARAMETER_COVERS);

        if (!isset($covers) || $covers <= 0) {
            throw new LackOfParameterException();
        }

        if ($vendor->getMaxCovers() < $covers) {
            return false;
        }

        return true;
    }
}

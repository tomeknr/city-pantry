<?php

declare(strict_types=1);

namespace App\Filter\Vendor;

use App\Entity\Vendor;
use App\Exception\LackOfParameterException;
use App\Parameter\ParameterBagInterface;

final class LocationFilter extends VendorFilter
{
    /**
     * @param Vendor $vendor
     * @param ParameterBagInterface $parameterBag
     * @return bool
     * @throws LackOfParameterException
     */
    public function filter(Vendor $vendor, ParameterBagInterface $parameterBag): bool
    {
        $postcode = $parameterBag->get(VendorFilter::PARAMETER_POSTCODE);

        if (!isset($postcode)) {
            throw new LackOfParameterException();
        }

        $postCodeShort = mb_substr($postcode, 0, 2);
        $position = strpos($vendor->getPostcode(), $postCodeShort);

        if ($position !== 0) {
            return false;
        }

        return true;
    }
}

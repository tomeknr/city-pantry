<?php

declare(strict_types=1);

namespace App\Filter\Vendor;

use App\Entity\Vendor;
use App\Parameter\ParameterBagInterface;

abstract class VendorFilter
{
    /** @var string  */
    public const PARAMETER_POSTCODE = 'postcode';
    /** @var string  */
    public const PARAMETER_COVERS = 'covers';

    /**
     * @param Vendor $vendor
     * @param ParameterBagInterface $parameterBag
     * @return bool
     */
    abstract public function filter(Vendor $vendor, ParameterBagInterface $parameterBag): bool;
}

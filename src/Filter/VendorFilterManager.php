<?php

declare(strict_types=1);

namespace App\Filter;

use App\Entity\Vendor;
use App\Filter\Vendor\VendorFilter;
use App\Parameter\ParameterBagInterface;

final class VendorFilterManager extends VendorFilter
{
    /** @var VendorFilter[] */
    private $collection = [];

    public function addFilter(VendorFilter $filter): void
    {
        $this->collection[] = $filter;
    }

    /**
     * @param Vendor $vendor
     * @param ParameterBagInterface $parameterBag
     * @return bool
     */
    public function filter(Vendor $vendor, ParameterBagInterface $parameterBag): bool
    {
        foreach ($this->collection as $filter) {
            if (!$filter->filter($vendor, $parameterBag)) {
                return false;
            }
        }

        return true;
    }
}

<?php

declare(strict_types=1);

namespace App\Input;

final class Input implements InputInterface
{
    /** @var array */
    private $arguments;

    /**
     * @param array $arguments
     */
    public function __construct(array $arguments)
    {
        $this->arguments = $arguments;
    }

    /**
     * @param int $index
     * @return null|string
     */
    public function getArgument(int $index): ?string
    {
        return isset($this->arguments[$index]) ? (string) $this->arguments[$index] : null;
    }

    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }
}

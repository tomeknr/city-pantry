<?php

declare(strict_types=1);

namespace App\Input;

interface InputInterface
{
    /**
     * @param int $index
     * @return null|string
     */
    public function getArgument(int $index): ?string;

    /**
     * @return array
     */
    public function getArguments(): array;
}

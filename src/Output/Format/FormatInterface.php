<?php

declare(strict_types=1);

namespace App\Output\Format;

interface FormatInterface
{
    /**
     * @param array $collection
     * @return array
     */
    public function getFormattedCollection(array $collection): array;
}

<?php

declare(strict_types=1);

namespace App\Output\Format;

use App\Entity\Item;
use App\Entity\Vendor;

class SimpleItemListFormat implements FormatInterface
{
    /** @var string  */
    public const SEPARATOR = ';';

    /**
     * @param Vendor[] $collection
     * @return array
     *
     * TODO it would be nice to change $collection array to object. Right now we are not sure what is inside the array
     */
    public function getFormattedCollection(array $collection): array
    {
        $results = [];

        foreach ($collection as $vendor) {

            // we have to be sure that we work with Vendor class
            if (!$vendor instanceof Vendor) {
                continue;
            }

            foreach ($vendor->getItems() as $item) {
                $results[] = $this->prepareItem($item);
            }
        }

        return $results;
    }

    /**
     * @param Item $item
     * @return string
     */
    private function prepareItem(Item $item): string
    {
        $allergies = empty($item->getAllergies()) ? self::SEPARATOR : implode(',', $item->getAllergies());
        $allergies = rtrim($allergies, ',');

        return $item->getName() . self::SEPARATOR . $allergies;
    }
}

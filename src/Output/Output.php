<?php

declare(strict_types=1);

namespace App\Output;

use App\Output\Format\FormatInterface;

final class Output implements OutputInterface
{
    /** @var string  */
    public const DEFAULT_TYPE = 'default_type';
    /** @var string  */
    public const ERROR_TYPE = 'error_type';

    /** @var FormatInterface */
    private $format;

    /**
     * Output constructor.
     * @param FormatInterface $format
     */
    public function __construct(FormatInterface $format)
    {
        $this->format = $format;
    }

    /**
     * @param string $text
     * @param string $messageType
     */
    public function print(string $text, string $messageType = self::DEFAULT_TYPE): void
    {
        switch ($messageType) {
            case self::ERROR_TYPE:
                $formattedTest = '[ERROR] ' . $text;
                break;
            case self::DEFAULT_TYPE:
            default:
                $formattedTest = $text;
        }

        echo $formattedTest . PHP_EOL;
    }

    /**
     * @param array $collection
     * @param string $messageType
     */
    public function printCollection(array $collection, string $messageType = self::DEFAULT_TYPE): void
    {
        foreach ($collection as $message) {
            $this->print($message, $messageType);
        }
    }

    /**
     * @param array $collection
     * @param string $messageType
     */
    public function printFormattedCollection(array $collection, string $messageType = self::DEFAULT_TYPE): void
    {
        $results = $this->format->getFormattedCollection($collection);
        $this->printCollection($results, $messageType);
    }
}

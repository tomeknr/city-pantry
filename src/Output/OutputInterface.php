<?php
declare(strict_types=1);

namespace App\Output;

interface OutputInterface
{
    /**
     * @param string $text
     * @param string $messageType
     */
    public function print(string $text, string $messageType): void;

    /**
     * @param array $collection
     * @param string $messageType
     */
    public function printCollection(array $collection, string $messageType): void;

    /**
     * @param array $collection
     * @param string $messageType
     */
    public function printFormattedCollection(array $collection, string $messageType): void;
}

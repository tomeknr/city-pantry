<?php

declare(strict_types=1);

namespace App\Parameter;

final class ParameterBag implements ParameterBagInterface
{
    /** @var array */
    private $parameters = [];

    /**
     * @param string $name
     * @return mixed|null
     */
    public function get(string $name)
    {
        return isset($this->parameters[$name]) ? $this->parameters[$name] : null;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->parameters;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return ParameterBagInterface
     */
    public function set(string $name, $value): ParameterBagInterface
    {
        $this->parameters[$name] = $value;

        return $this;
    }
}

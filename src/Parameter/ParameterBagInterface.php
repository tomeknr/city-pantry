<?php

declare(strict_types=1);

namespace App\Parameter;

interface ParameterBagInterface
{
    /**
     * @param string $name
     * @return mixed
     */
    public function get(string $name);

    /**
     * @return array
     */
    public function getAll(): array;

    /**
     * @param string $name
     * @param mixed $value
     * @return ParameterBagInterface
     */
    public function set(string $name, $value): ParameterBagInterface;
}

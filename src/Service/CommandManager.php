<?php

declare(strict_types=1);

namespace App\Service;

use App\Factory\Command\AvailabilityCommandFactory;
use App\Input\InputInterface;
use App\Output\OutputInterface;

final class CommandManager
{
    /** @var InputInterface */
    private $input;

    /** @var OutputInterface */
    private $output;

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function __construct(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
    }

    public function handle()
    {
        //TODO here we have only one command, but this is good place do use some abstraction when we want to add more commands
        (new AvailabilityCommandFactory())->getInstance()->run($this->input, $this->output);
    }
}

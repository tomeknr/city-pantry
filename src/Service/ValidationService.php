<?php

declare(strict_types=1);

namespace App\Service;

use App\Parameter\ParameterBagInterface;
use App\Validation\ParameterSetting;
use App\Validation\ValidationManagerInterface;
use App\Validation\ValidationVariableCollectionInterface;

final class ValidationService implements ValidationServiceInterface
{
    /** @var ValidationManagerInterface */
    private $validationManager;

    /** @var ValidationVariableCollectionInterface */
    private $validationVariableCollection;

    /**
     * @param ValidationManagerInterface $validationManager
     * @param ValidationVariableCollectionInterface $validationVariableCollection
     */
    public function __construct(
        ValidationManagerInterface $validationManager,
        ValidationVariableCollectionInterface $validationVariableCollection
    ) {
        $this->validationManager = $validationManager;
        $this->validationVariableCollection = $validationVariableCollection;
    }

    /**
     * @param ParameterBagInterface $parameterBag
     * @return bool
     */
    public function validate(ParameterBagInterface $parameterBag): bool
    {
        $this->validationManager->reset();

        foreach ($this->validationVariableCollection as $validationVariable) {
            $validationVariable->setValue($parameterBag->get($validationVariable->getName()));
        }

        $this->validationManager->setToValidation($this->validationVariableCollection);

        return $this->validationManager->validate();
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->validationManager->getMessages();
    }
}

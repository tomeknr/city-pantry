<?php

declare(strict_types=1);

namespace App\Service;

use App\Parameter\ParameterBagInterface;

interface ValidationServiceInterface
{
    /**
     * @param ParameterBagInterface $parameterBag
     * @return bool
     */
    public function validate(ParameterBagInterface $parameterBag): bool;

    /**
     * @return array
     */
    public function getMessages(): array;
}

<?php

declare(strict_types=1);

namespace App\Validation\Type;

class DateTimeFormatValidator extends Validator
{
    /** @var string  */
    public const DEFAULT_DATE_FORMAT = 'd/m/y';
    /** @var string  */
    public const DEFAULT_TIME_FORMAT = 'H:i';
    /** @var string */
    public const DEFAULT_MESSAGE = 'value is in incorrect format';

    /** @var string */
    private $format;

    /**
     * @param string $format
     * @param string|null $message
     */
    public function __construct(string $format, string $message = null)
    {
        $this->format = $format;
        parent::__construct($message);
    }

    /**
     * @param null|string $value
     * @return bool
     */
    public function validate(?string $value): bool
    {
        $date = \DateTime::createFromFormat($this->format, (string) $value);

        if (!$date instanceof \DateTime) {
            return false;
        }

        return true;
    }
}

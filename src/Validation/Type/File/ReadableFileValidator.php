<?php

declare(strict_types=1);

namespace App\Validation\Type\File;

use App\Validation\Type\Validator;

class ReadableFileValidator extends Validator
{
    /** @var string */
    public const DEFAULT_MESSAGE = 'file doesn\'t exist or is not writable';

    /**
     * @param null|string $value
     * @return bool
     */
    public function validate(?string $value): bool
    {
        if (!file_exists((string) $value) || !is_readable((string) $value)) {
            return false;
        }

        return true;
    }
}

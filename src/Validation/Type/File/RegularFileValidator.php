<?php

declare(strict_types=1);

namespace App\Validation\Type\File;

use App\Validation\Type\Validator;

class RegularFileValidator extends Validator
{
    /** @var string */
    public const DEFAULT_MESSAGE = 'file is not a regular file';

    /**
     * @param null|string $value
     * @return bool
     */
    public function validate(?string $value): bool
    {
        if (!is_file((string) $value)) {
            return false;
        }

        return true;
    }
}

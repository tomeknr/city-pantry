<?php

declare(strict_types=1);

namespace App\Validation\Type;

class NumberValidator extends Validator
{
    /** @var string */
    public const DEFAULT_MESSAGE = 'value has to be a digit greater than zero';

    /**
     * @param null|string $value
     * @return bool
     */
    public function validate(?string $value): bool
    {
        if (!ctype_digit((string) $value) || $value <= 0) {
            return false;
        }

        return true;
    }
}

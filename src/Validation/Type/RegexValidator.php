<?php

declare(strict_types=1);

namespace App\Validation\Type;

class RegexValidator extends Validator
{
    public const POSTCODE_FORMAT = '/^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|
    (([AZa-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z]))))[0-9][A-Za-z]{2})$/';

    /** @var string */
    public const DEFAULT_MESSAGE = 'value is in incorrect format';

    /** @var string */
    private $format;

    /**
     * @param string $format
     * @param string $message
     */
    public function __construct(string $format, string $message = null)
    {
        $this->format = $format;
        parent::__construct($message);
    }

    /**
     * @param null|string $value
     * @return bool
     */
    public function validate(?string $value): bool
    {
        if (!preg_match($this->format, (string) $value)) {
            return false;
        }

        return true;
    }
}

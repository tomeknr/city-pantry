<?php

declare(strict_types=1);

namespace App\Validation\Type;

class RequiredValidator extends Validator
{
    /** @var string */
    public const DEFAULT_MESSAGE = 'value is required';

    /**
     * @param null|string $value
     * @return bool
     */
    public function validate(?string $value): bool
    {
        if (!isset($value)) {
            return false;
        }

        return true;
    }
}

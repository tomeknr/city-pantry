<?php

declare(strict_types=1);

namespace App\Validation\Type;

abstract class Validator
{
    /** @var string */
    public const DEFAULT_MESSAGE = 'value is incorrect';

    /** @var string */
    protected $prefix = 'Argument %s: %s';
    /** @var string */
    protected $message;

    /**
     * @param null|string $value
     * @return bool
     */
    abstract public function validate(?string $value): bool;

    /**
     * @param null|string $message
     */
    public function __construct(?string $message = null)
    {
        if ($message) {
            $this->message = $message;

            return;
        }

        $this->message = static::DEFAULT_MESSAGE;
    }

    /**
     * @param string $name
     * @return string
     */
    public function getMessage(string $name): string
    {
        return sprintf($this->prefix, $name, $this->message);
    }
}

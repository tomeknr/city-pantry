<?php

declare(strict_types=1);

namespace App\Validation;

final class ValidationManager implements ValidationManagerInterface
{
    /** @var array */
    private $messages = [];

    /** @var ValidationVariableCollectionInterface[] */
    private $collection;

    /**
     * @param ValidationVariableCollectionInterface $collection
     */
    public function setToValidation(ValidationVariableCollectionInterface $collection): void
    {
        $this->collection = $collection;
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        if (!$this->collection instanceof ValidationVariableCollectionInterface) {
            return true;
        }

        foreach ($this->collection as $item) {
            $this->validateParameter($item);
        }

        return empty($this->messages) ? true : false;
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    public function reset(): void
    {
        $this->messages = [];
    }

    /**
     * @param ValidationVariableInterface $parameter
     */
    private function validateParameter(ValidationVariableInterface $parameter)
    {
        $value = $parameter->getValue();

        foreach ($parameter->getValidators() as $validator) {
            if (!$validator->validate($value)) {
                $this->addMessage($validator->getMessage($parameter->getName(), $value));
            }
        }
    }

    /**
     * @param string $message
     */
    private function addMessage(string $message): void
    {
        $this->messages[] = $message;
    }
}

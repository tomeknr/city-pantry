<?php

declare(strict_types=1);

namespace App\Validation;

interface ValidationManagerInterface
{
    /**
     * @param ValidationVariableCollectionInterface $collection
     */
    public function setToValidation(ValidationVariableCollectionInterface $collection): void;

    /**
     * @return bool
     */
    public function validate(): bool;

    /**
     * @return array
     */
    public function getMessages(): array;

    public function reset(): void;
}

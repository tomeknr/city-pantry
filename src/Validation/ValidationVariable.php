<?php

declare(strict_types=1);

namespace App\Validation;

use App\Validation\Type\Validator;

final class ValidationVariable implements ValidationVariableInterface
{
    /** @var string */
    private $name;

    /** @var string */
    private $value;

    /** @var array */
    private $validators;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ValidationVariable
     */
    public function setName(string $name): ValidationVariableInterface
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return ValidationVariable
     */
    public function setValue(?string $value): ValidationVariableInterface
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return array
     */
    public function getValidators(): array
    {
        return $this->validators;
    }

    /**
     * @param Validator $validator
     * @return ValidationVariable
     */
    public function addValidator(Validator $validator): ValidationVariableInterface
    {
        $this->validators[] = $validator;

        return $this;
    }

    /**
     * @param array $validators
     * @return ValidationVariable
     */
    public function setValidators(array $validators): ValidationVariableInterface
    {
        foreach ($validators as $validator) {
            $this->addValidator($validator);
        }

        return $this;
    }
}

<?php

declare(strict_types=1);

namespace App\Validation;

final class ValidationVariableCollection implements \Iterator, ValidationVariableCollectionInterface
{
    /** @var ValidationVariableInterface[] */
    private $collection = [];

    /**
     * @param ValidationVariableInterface $item
     * @return ValidationVariableCollection
     */
    public function add(ValidationVariableInterface $item): ValidationVariableCollection
    {
        $this->collection[] = $item;

        return $this;
    }
    /**
     * @return mixed
     */
    public function current()
    {
        return current($this->collection);
    }

    /**
     * @return mixed|void
     */
    public function next()
    {
        return next($this->collection);
    }

    /**
     * @return mixed
     */
    public function key()
    {
        return key($this->collection);
    }

    /**
     * @return bool
     */
    public function valid()
    {
        $key = key($this->collection);

        return ($key !== null && $key !== false);
    }

    public function rewind()
    {
        reset($this->collection);
    }
}

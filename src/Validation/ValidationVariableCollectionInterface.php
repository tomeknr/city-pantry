<?php

declare(strict_types=1);

namespace App\Validation;

interface ValidationVariableCollectionInterface
{
    /**
     * @param ValidationVariableInterface $item
     * @return ValidationVariableCollection
     */
    public function add(ValidationVariableInterface $item): ValidationVariableCollection;
}

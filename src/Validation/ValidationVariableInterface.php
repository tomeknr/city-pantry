<?php

declare(strict_types=1);

namespace App\Validation;

use App\Validation\Type\Validator;

interface ValidationVariableInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return ValidationVariableInterface
     */
    public function setName(string $name): ValidationVariableInterface;

    /**
     * @return null|string
     */
    public function getValue(): ?string;

    /**
     * @param null|string $value
     * @return ValidationVariableInterface
     */
    public function setValue(?string $value): ValidationVariableInterface;

    /**
     * @return array
     */
    public function getValidators(): array;

    /**
     * @param Validator $validator
     * @return ValidationVariableInterface
     */
    public function addValidator(Validator $validator): ValidationVariableInterface;

    /**
     * @param array $validators
     * @return ValidationVariableInterface
     */
    public function setValidators(array $validators): ValidationVariableInterface;
}

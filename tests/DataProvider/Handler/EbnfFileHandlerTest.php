<?php

declare(strict_types=1);

namespace Test\Filter\Vendor;

use App\DataProvider\Handler\EbnfFileHandler;
use App\Entity\Item;
use App\Entity\Vendor;
use App\Filter\Vendor\CoverFilter;
use App\Parameter\ParameterBagInterface;
use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;

class EbnfFileHandlerTest extends TestCase
{
    /** @var array */
    private $preparedData;

    /** @var MockInterface */
    private $parameterBagMock;

    /** @var CoverFilter */
    private $handler;

    protected function setUp(): void
    {
        $this->parameterBagMock = Mockery::mock(ParameterBagInterface::class);
        $this->handler = new EbnfFileHandler();

        $this->preparedData = [
            [
                'name' => 'Ghana Kitchen',
                'postcode' => 'NW42QA',
                'maxCovers' => 40,
                'items' => [
                    [
                        'name' => 'Premium meat selection',
                        'allergies' => [],
                        'advanceTime' => '36h'
                    ],
                    [
                        'name' => 'Breakfast',
                        'allergies' => [
                            'gluten',
                            'eggs'
                        ],
                        'advanceTime' => '12h'
                    ],
                ]
            ],
            [
                'name' => 'Well Kneaded',
                'postcode' => 'EC32BA',
                'maxCovers' => 150,
                'items' => [
                    [
                        'name' => 'Full English breakfast',
                        'allergies' => [
                            'gluten'
                        ],
                        'advanceTime' => '24h'
                    ],
                ]
            ],
            [
                'name' => 'Well Kneaded 2',
                'postcode' => 'EC32BB',
                'maxCovers' => 12,
                'items' => []
            ],
        ];
    }

    /**
     * @return array
     */
    protected function generateCollection()
    {
        $collection = [];

        foreach ($this->preparedData as $vendorData) {
            $vendor = new Vendor();
            $vendor
                ->setName($vendorData['name'])
                ->setPostcode($vendorData['postcode'])
                ->setMaxCovers($vendorData['maxCovers']);

            foreach ($vendorData['items'] as $itemData) {
                $item = new Item();
                $item
                    ->setName($itemData['name'])
                    ->setAllergies($itemData['allergies'])
                    ->setAdvanceTime($itemData['advanceTime']);

                $vendor->addItem($item);
            }

            if (count($vendor->getItems()) !== 0) {
                $collection[] = $vendor;
            }
        }

        return $collection;
    }

    /**
     * @return array
     */
    protected function generateFormatData()
    {
        $data = [];
        $format = '%s;%s;%s';

        foreach ($this->preparedData as $vendorData) {
            $data[] = sprintf($format, $vendorData['name'], $vendorData['postcode'], $vendorData['maxCovers']);

            foreach ($vendorData['items'] as $itemData) {
                $allergies = '';

                if (count($itemData['allergies']) > 0) {
                    $allergies = rtrim(implode(',', $itemData['allergies']), ',');
                }

                $data[] = sprintf($format, $itemData['name'], $allergies, $itemData['advanceTime']);
            }

            $data[] = "\r\n";
        }

        array_pop($data);

        return $data;
    }

    public function testFileHandler(): void
    {
        foreach ($this->generateFormatData() as $data) {
            $this->handler->handleFile($data, $this->parameterBagMock);
        }

        $collection = $this->handler->getCollection();
        $preparedCollection = $this->generateCollection();

        $this->assertEquals(count($collection), count($preparedCollection));

        foreach ($preparedCollection as $index => $preparedVendor) {
            if (!isset($collection[$index])) {
                break;
            };

            $preparedVendorSerialized = serialize($preparedVendor);
            $vendorSerialized = serialize($collection[$index]);

            $this->assertEquals($preparedVendorSerialized, $vendorSerialized);
        }
    }
}

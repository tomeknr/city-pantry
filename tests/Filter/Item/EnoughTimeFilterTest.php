<?php

declare(strict_types=1);

namespace Test\Filter\Vendor;

use App\Entity\Item;
use App\Filter\Item\EnoughTimeFilter;
use App\Filter\Item\ItemFilter;
use App\Parameter\ParameterBagInterface;
use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;

class EnoughTimeFilterTest extends TestCase
{
    // do not change it because tests based on it
    public const DATETIME = '20-12-2018 10:00';

    /** @var MockInterface */
    private $itemMock;

    /** @var MockInterface */
    private $parameterBagMock;

    /** @var EnoughTimeFilter */
    private $enoughTimeFilter;

    protected function setUp(): void
    {
        $this->itemMock = Mockery::mock(Item::class);
        $this->parameterBagMock = Mockery::mock(ParameterBagInterface::class);
        $this->enoughTimeFilter = new EnoughTimeFilter(new \DateTime(self::DATETIME));
    }

    protected function tearDown()
    {
        Mockery::close();
    }

    /**
     * @param string $advanceTime
     */
    private function setUpItemMock(string $advanceTime): void
    {
        $this
            ->itemMock
            ->shouldReceive('getAdvanceTime')
            ->andReturn($advanceTime);
    }

    /**
     * @param string $parameter
     * @param mixed $value
     */
    private function setUpParameterBagMock(string $parameter, $value)
    {
        $this
            ->parameterBagMock
            ->shouldReceive('get')
            ->with($parameter)
            ->times(1)
            ->andReturn($value);
    }

    /**
     * @param string|null $fileDate
     * @param string|null $fileTime
     * @throws \App\Exception\LackOfParameterException
     *
     * @dataProvider exceptionDataProvider
     * @expectedException App\Exception\LackOfParameterException
     */
    public function testFilterException(?string $fileDate, ?string $fileTime): void
    {
        $this->setUpParameterBagMock(ItemFilter::PARAMETER_DATE, $fileDate);
        $this->setUpParameterBagMock(ItemFilter::PARAMETER_TIME, $fileTime);

        $this->enoughTimeFilter->filter($this->itemMock, $this->parameterBagMock);
    }

    /**
     * @return \Generator
     */
    public function exceptionDataProvider()
    {
        yield ['20/12/18', null];
        yield [null, '20/12/18'];
    }

    /**
     * @param string $fileDate
     * @param string $fileTime
     * @param string $advanceTime
     * @param bool $providerResult
     * @throws \App\Exception\LackOfParameterException
     *
     * @dataProvider standardDataProvider
     */
    public function testFilter(string $fileDate, string $fileTime, string $advanceTime, bool $providerResult): void
    {
        $this->setUpParameterBagMock(ItemFilter::PARAMETER_DATE, $fileDate);
        $this->setUpParameterBagMock(ItemFilter::PARAMETER_TIME, $fileTime);
        $this->setUpItemMock($advanceTime);

        $result = $this->enoughTimeFilter->filter($this->itemMock, $this->parameterBagMock);

        $this->assertEquals($result, $providerResult);
    }

    /**
     * @return \Generator
     */
    public function standardDataProvider()
    {
        yield ['20/12/18', '14:00', '4h', true];   // minimal correct value
        yield ['20/12/18', '13:59', '4h', false];  // maximal incorrect value
        yield ['21/12/18', '11:00', '4h', true];   // in future
        yield ['19/12/18', '10:00', '4h', false];  // date in the past
        yield ['20/12/18', '05:00', '4h', false];  // time in the past
        yield ['22/12/18', '09:00', '47h', true];  // minimal correct value (with days)
        yield ['22/12/18', '08:59', '47h', false]; // maximal incorrect value (with days)
    }
}

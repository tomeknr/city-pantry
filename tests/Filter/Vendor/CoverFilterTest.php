<?php

declare(strict_types=1);

namespace Test\Filter\Vendor;

use App\Entity\Vendor;
use App\Filter\Vendor\CoverFilter;
use App\Parameter\ParameterBagInterface;
use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;

class CoverFilterTest extends TestCase
{
    /** @var MockInterface */
    private $vendorMock;

    /** @var MockInterface */
    private $parameterBagMock;

    /** @var CoverFilter */
    private $coverFilter;

    protected function setUp(): void
    {
        $this->vendorMock = Mockery::mock(Vendor::class);
        $this->parameterBagMock = Mockery::mock(ParameterBagInterface::class);
        $this->coverFilter = new CoverFilter();
    }

    protected function tearDown()
    {
        Mockery::close();
    }

    /**
     * @param int $results
     */
    private function setUpVendorMock(int $results): void
    {
        $this
            ->vendorMock
            ->shouldReceive('getMaxCovers')
            ->times(1)
            ->andReturn($results);
    }

    /**
     * @param mixed $results
     */
    private function setUpParameterBagMock($results)
    {
        $this
            ->parameterBagMock
            ->shouldReceive('get')
            ->times(1)
            ->andReturn($results);
    }

    /**
     * @param mixed $consoleInput
     * @throws \App\Exception\LackOfParameterException
     *
     * @dataProvider exceptionDataProvider
     * @expectedException App\Exception\LackOfParameterException
     */
    public function testFilterException($consoleInput): void
    {
        $this->setUpParameterBagMock($consoleInput);

        $this->coverFilter->filter($this->vendorMock, $this->parameterBagMock);
    }

    /**
     * @return \Generator
     */
    public function exceptionDataProvider()
    {
        yield [null];
        yield [0];
        yield ['test'];
        yield [-10];
    }

    /**
     * @param int $fileValue
     * @param mixed $consoleInput
     * @param bool $providerResult
     * @throws \App\Exception\LackOfParameterException
     *
     * @dataProvider standardDataProvider
     */
    public function testFilter(int $fileValue, $consoleInput, bool $providerResult): void
    {
        $this->setUpVendorMock($fileValue);
        $this->setUpParameterBagMock($consoleInput);

        $result = $this->coverFilter->filter($this->vendorMock, $this->parameterBagMock);

        $this->assertEquals($result, $providerResult);
    }

    /**
     * @return \Generator
     */
    public function standardDataProvider()
    {
        yield [10, 10, true];
        yield [11, 10, true];
        yield [10, 11, false];
        yield [10, 12.5, false];
        yield [10, '14test', false];
        yield [10, '8test', true];
    }
}

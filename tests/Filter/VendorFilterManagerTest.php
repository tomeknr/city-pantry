<?php

declare(strict_types=1);

namespace Test\Filter\Vendor;

use App\Entity\Vendor;
use App\Filter\Vendor\VendorFilter;
use App\Filter\VendorFilterManager;
use App\Parameter\ParameterBagInterface;
use Mockery;
use PHPUnit\Framework\TestCase;
use Mockery\MockInterface;

class VendorFilterManagerTest extends TestCase
{
    /** @var MockInterface */
    private $vendorMock;

    /** @var MockInterface */
    private $parameterBagMock;

    /** @var MockInterface */
    private $vendorFilterMock;

    /** @var VendorFilterManager */
    private $vendorFilterManager;

    protected function setUp(): void
    {
        $this->vendorMock = Mockery::mock(Vendor::class);
        $this->parameterBagMock = Mockery::mock(ParameterBagInterface::class);
        $this->vendorFilterMock = Mockery::mock(VendorFilter::class);
        $this->vendorFilterManager = new VendorFilterManager();
    }

    /**
     * @param mixed $results
     */
    private function setUpVendorFilterMock($results): void
    {
        $this
            ->vendorFilterMock
            ->shouldReceive('filter')
            ->times(1)
            ->andReturn($results);
    }

    /**
     * @param bool $filterResult
     * @param bool $providerResult
     *
     * @dataProvider dataProvider
     */
    public function testFilter(bool $filterResult, bool $providerResult): void
    {
        $this->setUpVendorFilterMock($filterResult);
        $this->vendorFilterManager->addFilter($this->vendorFilterMock);

        $result = $this->vendorFilterManager->filter($this->vendorMock, $this->parameterBagMock);
        $this->assertEquals($result, $providerResult);
    }

    /**
     * @return \Generator
     */
    public function dataProvider()
    {
        yield [true, true];
        yield [false, false];
    }
}

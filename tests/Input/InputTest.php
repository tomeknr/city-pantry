<?php
declare(strict_types=1);

namespace Test\Filter\Vendor;

use App\Input\Input;
use PHPUnit\Framework\TestCase;

class InputTest extends TestCase
{
    public function testGetArguments(): void
    {
        $arguments = ['one', 'two', '', null, true];
        $input = new Input($arguments);
        $this->assertEquals($input->getArguments(), $arguments);
    }

    /**
     * @param array $arguments
     * @param int $index
     * @param mixed $providerResult
     *
     * @dataProvider dataProvider
     */
    public function testGetArgument(array $arguments, int $index, $providerResult): void
    {
        $input = new Input($arguments);
        $result = $input->getArgument($index);
        $this->assertEquals($result, $providerResult);
    }

    /**
     * @return \Generator
     */
    public function dataProvider()
    {
        $params = ['one', 'two', '', null, true];

        yield [$params, 0, $params[0]];
        yield [$params, 1, $params[1]];
        yield [$params, 2, $params[2]];
        yield [$params, 3, $params[3]];
        yield [$params, 4, $params[4]];
        yield [$params, 10, null];
    }
}

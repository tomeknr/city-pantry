<?php
declare(strict_types=1);

namespace Test\Filter\Vendor;

use App\Parameter\ParameterBagInterface;
use App\Service\ValidationService;
use App\Validation\ValidationManagerInterface;
use App\Validation\ValidationVariableCollectionInterface;
use PHPUnit\Framework\TestCase;
use Mockery\MockInterface;
use Mockery;

class ValidationServiceTest extends TestCase
{
    /** @var MockInterface */
    private $validationManagerMock;

    /** @var MockInterface */
    private $parameterBagMock;

    /** @var MockInterface */
    private $validationVariableCollectionMock;

    /** @var ValidationService */
    private $validationService;

    protected function setUp(): void
    {
        $this->validationManagerMock = Mockery::mock(ValidationManagerInterface::class);
        $this->parameterBagMock = Mockery::mock(ParameterBagInterface::class);
        $this->validationVariableCollectionMock = Mockery::mock(ValidationVariableCollectionInterface::class)->makePartial();
        $this->validationService = new ValidationService($this->validationManagerMock, $this->validationVariableCollectionMock);
    }

    private function setUpValidationManagerMock($results): void
    {
        $this
            ->validationManagerMock
            ->shouldReceive('reset')
            ->times(1);

        $this
            ->validationManagerMock
            ->shouldReceive('setToValidation')
            ->with($this->validationVariableCollectionMock)
            ->times(1);

        $this
            ->validationManagerMock
            ->shouldReceive('validate')
            ->times(1)
            ->andReturn($results);
    }

    private function setUpParameterBagMock(): void
    {
        $this
            ->parameterBagMock
            ->shouldReceive('get')
            ->times(1)
            ->andReturn(1);
    }


    private function setUpValidationVariableCollectionMock(): void
    {
        $this
            ->validationVariableCollectionMock
            ->shouldReceive('get')
            ->times(1)
            ->andReturn(1);
    }

    /**
     * @param bool $filterResult
     * @param bool $providerResult
     *
     * @dataProvider dataProvider
     */
    public function testValidate(bool $filterResult, bool $providerResult): void
    {
        $this->setUpValidationManagerMock($filterResult);
        $this->setUpParameterBagMock();
        $this->setUpValidationVariableCollectionMock();

        $result = $this->validationService->validate($this->parameterBagMock);
        $this->assertEquals($result, $providerResult);
    }

    /**
     * @return \Generator
     */
    public function dataProvider()
    {
        yield [true, true];
        yield [false, false];
    }
}
